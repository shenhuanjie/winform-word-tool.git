﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordOpenXmlClassLibrary.Utils
{
    /// <summary>
    /// 文件路径工具类
    /// </summary>
    public static class FilePathUtil
    {

        public static string DocumentFilePath()
        {
            string dateTime = DateTimeUtil.DateTimeToTimeStamp(DateTime.Now).ToString();
            string dateStamp = DateTimeUtil.DateStampString(DateTime.Now);

            string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);//获取桌面目录
            string currentDirectory = Environment.CurrentDirectory.ToString();//获取项目根目录

            string fileName = dateTime + ".docx";

            string fileDir = currentDirectory + "\\" + dateStamp;
            string filePath = fileDir + "\\" + fileName;

            // 创建目录
            DirectoryInfo newDirectory = new DirectoryInfo(fileDir);
            newDirectory.Create();

            return filePath;
        }

    }
}
