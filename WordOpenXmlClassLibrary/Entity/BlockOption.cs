﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordOpenXmlClassLibrary.Entity
{
    public class BlockOption
    {
        private int blockType;// 题目类型

        private string titleName;// 题目标题
        private bool titleCenter;// 题目标题居中

        private int lineNumber;// 作文行数
        private int wordNumber;// 作文字数

        private string optionSequence;// 选项序列
        private int numberOfQuestions;// 题目数量
        private int startingQuestionNumber;// 起始题号
        private int topicGroupDirection;// 题组方向

        private string subjectiveItemContent;// 题块内容
        private bool boldDisplay;// 是否显示边框
        private int borderWidth;// 边框宽度

        public BlockOption()
        {
            this.blockType = 0;

            this.titleName = "";
            this.titleCenter = false;

            this.lineNumber = 0;
            this.wordNumber = 0;

            this.optionSequence = "A,B,C,D";
            this.numberOfQuestions = 1;
            this.startingQuestionNumber = 1;
            this.topicGroupDirection = 0;

            this.subjectiveItemContent = "";
            this.boldDisplay = false;
            this.borderWidth = 0;
        }

        public void AddItem(string titleName, bool titleCenter, int lineNumber, int wordNumber, string optionSequence, int numberOfQuestions, int startingQuestionNumber, int topicGroupDirection, string subjectiveItemContent, bool boldDisplay, int borderWidth)
        {
            TitleName = titleName ?? throw new ArgumentNullException(nameof(titleName));
            TitleCenter = titleCenter;
            LineNumber = lineNumber;
            WordNumber = wordNumber;
            OptionSequence = optionSequence ?? throw new ArgumentNullException(nameof(optionSequence));
            NumberOfQuestions = numberOfQuestions;
            StartingQuestionNumber = startingQuestionNumber;
            TopicGroupDirection = topicGroupDirection;
            SubjectiveItemContent = subjectiveItemContent ?? throw new ArgumentNullException(nameof(subjectiveItemContent));
            BoldDisplay = boldDisplay;
            BorderWidth = borderWidth;
        }


        public string TitleName { get => titleName; set => titleName = value; }
        public bool TitleCenter { get => titleCenter; set => titleCenter = value; }
        public int LineNumber { get => lineNumber; set => lineNumber = value; }
        public int WordNumber { get => wordNumber; set => wordNumber = value; }
        public string OptionSequence { get => optionSequence; set => optionSequence = value; }
        public int NumberOfQuestions { get => numberOfQuestions; set => numberOfQuestions = value; }
        public int StartingQuestionNumber { get => startingQuestionNumber; set => startingQuestionNumber = value; }
        public int TopicGroupDirection { get => topicGroupDirection; set => topicGroupDirection = value; }
        public string SubjectiveItemContent { get => subjectiveItemContent; set => subjectiveItemContent = value; }
        public bool BoldDisplay { get => boldDisplay; set => boldDisplay = value; }
        public int BorderWidth { get => borderWidth; set => borderWidth = value; }
        public int BlockType { get => blockType; set => blockType = value; }

    }
}
