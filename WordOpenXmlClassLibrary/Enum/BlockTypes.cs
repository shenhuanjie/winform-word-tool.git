﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordOpenXmlClassLibrary.Enum
{
    public enum BlockTypes
    {
        Default = 0,
        Objective = 1,
        Subjective = 2,
        English = 3,
        Chinese = 4
    }
}
