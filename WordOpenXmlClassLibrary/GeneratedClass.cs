﻿using System;
using System.Collections;
using System.Collections.Generic;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using WordOpenXmlClassLibrary.Entity;
using WordOpenXmlClassLibrary.Enum;

namespace WordOpenXmlClassLibrary
{
    public class GeneratedClass
    {
        private string filePath;
        private string examName;
        private PageSizeValues pageSize;
        private PageOrientationValues pageOrientation;

        public string FilePath { get => filePath; set => filePath = value; }
        public string ExamName { get => examName; set => examName = value; }
        public PageSizeValues PageSize { get => pageSize; set => pageSize = value; }
        public PageOrientationValues PageOrientation { get => pageOrientation; set => pageOrientation = value; }

        public bool Create(List<BlockOption> blockOptions)
        {
            using (WordprocessingDocument wordprocessingDocument = WordprocessingDocument.Create(FilePath, WordprocessingDocumentType.Document))
            {
                // 创建基础文档结构
                Generater generater = new Generater(wordprocessingDocument);
                generater.ExamTitle(ExamName);
                generater.ExaminationNo();
                generater.StudentInfo();

                foreach (BlockOption option in blockOptions)
                {
                    int blockType = option.BlockType;// 题目类型

                    string titleName = option.TitleName;// 题目标题
                    bool titleCenter = option.TitleCenter;// 题目标题居中

                    int lineNumber = option.LineNumber;// 作文行数
                    int wordNumber = option.WordNumber;// 作文字数

                    string optionSequence = option.OptionSequence;// 选项序列
                    int numberOfQuestions = option.NumberOfQuestions;// 题目数量
                    int startingQuestionNumber = option.StartingQuestionNumber;// 起始题号
                    int topicGroupDirection = option.TopicGroupDirection;// 题组方向

                    string subjectiveItemContent = option.SubjectiveItemContent;// 题块内容
                    bool boldDisplay = option.BoldDisplay;// 是否显示边框
                    int borderWidth = option.BorderWidth;// 边框宽度

                    switch (blockType)
                    {
                        case (int)BlockTypes.Default:
                            if (titleCenter)
                            {
                                generater.ModuleTitle(titleName);
                            }
                            else
                            {
                                generater.HeaderTitle(titleName);
                            }
                            break;

                        case (int)BlockTypes.Objective:

                            PageOrientationValues orientationValue = new PageOrientationValues();
                            switch (topicGroupDirection)
                            {
                                case (int)PageOrientationValues.Landscape:
                                    orientationValue = PageOrientationValues.Landscape;
                                    break;
                                case (int)PageOrientationValues.Portrait:
                                    orientationValue = PageOrientationValues.Portrait;
                                    break;
                                default:
                                    orientationValue = PageOrientationValues.Landscape;
                                    break;

                            }
                            generater.HeaderTitle(titleName);
                            generater.ObjectiveItem(optionSequence.Split(','), numberOfQuestions, startingQuestionNumber, orientationValue);
                            break;

                        case (int)BlockTypes.Subjective:

                            int borderSize = 0;
                            int extendLine = 1;
                            if (boldDisplay)
                            {
                                borderSize = borderWidth;
                            }

                            generater.HeaderTitle(titleName);
                            generater.SubjectiveItem(subjectiveItemContent, borderSize, extendLine);
                            break;
                        case (int)BlockTypes.English:
                            if (titleCenter)
                            {
                                generater.ModuleTitle(titleName);
                            }
                            else
                            {
                                generater.HeaderTitle(titleName);
                            }
                            generater.SubjectiveItem(subjectiveItemContent, 0, 1);
                            generater.CreateEnglishWriting(lineNumber);
                            break;
                        case (int)BlockTypes.Chinese:

                            if (titleCenter)
                            {
                                generater.ModuleTitle(titleName);
                            }
                            else
                            {
                                generater.HeaderTitle(titleName);
                            }
                            generater.SubjectiveItem(subjectiveItemContent, 0, 1);
                            generater.CreateWriting(lineNumber);
                            break;
                    }
                }
                //// 设置页面大小：默认A4
                generater.SectionProperties(PageSize, PageOrientation);
                // 保存文档
                generater.Save();
            }
            return true;
        }
    }
}
