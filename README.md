### 项目概况

* 项目地址：https://gitee.com/shenhuanjie/winform-word-tool.git

* 项目名称：winform-word-tool
* 项目描述：一个基于Windows 窗体应用(.NET Framework)的答题卡生成工具

### 需求概况：

* 支持快速生成答题卡
* 支持导出Word文档文件
* 支持实时预览
* 支持客观题（选择题）、主观题（填空题、作答题、英语作文、语文作文）等题块的生成。
* 支持生成题块的快速编辑功能
* 支持实时打印
* 导出PDF等多种基础文档格式文件

### 快速开始

```shell
git clone https://gitee.com/shenhuanjie/winform-word-tool.git
```

### 相关依赖

* .NET Framework 4.7.2 (v.4.0+)

* Open XML SDK 
* Spire.Doc
* Spire.DocViewer

### 参考文档

* https://docs.microsoft.com/zh-cn/office/open-xml/open-xml-sdk
* http://www.e-iceblue.cn/Introduce/Spire-Doc-NET.html