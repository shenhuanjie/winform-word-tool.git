﻿using DocumentFormat.OpenXml.Wordprocessing;
using WordOpenXmlClassLibrary.Enum;

namespace WordOpenXmlFormApp
{
    partial class ApplicationForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ApplicationForm));
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.新建NToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.打开OToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.保存SToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.打印PToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.剪切UToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.复制CToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.粘贴PToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.帮助LToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelTip = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelTool = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.groupBoxCenter = new System.Windows.Forms.GroupBox();
            this.groupBoxOption = new System.Windows.Forms.GroupBox();
            this.panelSpan4 = new System.Windows.Forms.Panel();
            this.panelOptionBlockObjectiveItemBold = new System.Windows.Forms.Panel();
            this.checkBoxShowBold = new System.Windows.Forms.CheckBox();
            this.numericUpDownBlockBoldSize = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.panelOptionBlockSubjectiveItem = new System.Windows.Forms.Panel();
            this.richTextBoxBlockSubjectiveItem = new System.Windows.Forms.RichTextBox();
            this.panelSpan3 = new System.Windows.Forms.Panel();
            this.panelOptionBlockObjectiveItem = new System.Windows.Forms.Panel();
            this.comboBoxObjectiveItemOrientation = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.numericUpDownQuestionStart = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.numericUpDownQuestionCount = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxBlockOptionSequence = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.panelSpan2 = new System.Windows.Forms.Panel();
            this.panelOptionBlockWritting = new System.Windows.Forms.Panel();
            this.numericUpDownWordCount = new System.Windows.Forms.NumericUpDown();
            this.labelBlockOptionWordCount = new System.Windows.Forms.Label();
            this.numericUpDownWordLine = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.panelSpan1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.buttonBlockOptionSave = new System.Windows.Forms.Button();
            this.panelOptionBlockName = new System.Windows.Forms.Panel();
            this.checkBoxBlockNameTitleCenter = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxBlockNameTitle = new System.Windows.Forms.TextBox();
            this.groupBoxExamInfo = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxExamName = new System.Windows.Forms.TextBox();
            this.groupBoxBlockList = new System.Windows.Forms.GroupBox();
            this.buttonAddNodeBlock = new System.Windows.Forms.Button();
            this.panelTreeViewOption = new System.Windows.Forms.Panel();
            this.buttonNodeDelete = new System.Windows.Forms.Button();
            this.buttonNodeDown = new System.Windows.Forms.Button();
            this.buttonNodeUp = new System.Windows.Forms.Button();
            this.treeViewBlock = new System.Windows.Forms.TreeView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.comboBoxPageSize = new System.Windows.Forms.ComboBox();
            this.comboBoxPageOrientation = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBoxBlockType = new System.Windows.Forms.GroupBox();
            this.buttonChineseWriting = new System.Windows.Forms.Button();
            this.panel9 = new System.Windows.Forms.Panel();
            this.buttonEnglishWriting = new System.Windows.Forms.Button();
            this.panel8 = new System.Windows.Forms.Panel();
            this.buttonSubjectiveItem = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.buttonObjectiveItem = new System.Windows.Forms.Button();
            this.buttonPreviewFile = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.docDocumentViewer = new Spire.DocViewer.Forms.DocDocumentViewer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonZoomToDown = new System.Windows.Forms.Button();
            this.buttonZoomToUp = new System.Windows.Forms.Button();
            this.buttonExportFile = new System.Windows.Forms.Button();
            this.buttonExit = new System.Windows.Forms.Button();
            this.toolStrip.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.groupBoxCenter.SuspendLayout();
            this.groupBoxOption.SuspendLayout();
            this.panelOptionBlockObjectiveItemBold.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBlockBoldSize)).BeginInit();
            this.panelOptionBlockSubjectiveItem.SuspendLayout();
            this.panelOptionBlockObjectiveItem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownQuestionStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownQuestionCount)).BeginInit();
            this.panelOptionBlockWritting.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownWordCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownWordLine)).BeginInit();
            this.panel3.SuspendLayout();
            this.panelOptionBlockName.SuspendLayout();
            this.groupBoxExamInfo.SuspendLayout();
            this.groupBoxBlockList.SuspendLayout();
            this.panelTreeViewOption.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBoxBlockType.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip
            // 
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.新建NToolStripButton,
            this.打开OToolStripButton,
            this.保存SToolStripButton,
            this.打印PToolStripButton,
            this.toolStripSeparator,
            this.剪切UToolStripButton,
            this.复制CToolStripButton,
            this.粘贴PToolStripButton,
            this.toolStripSeparator1,
            this.帮助LToolStripButton});
            this.toolStrip.Location = new System.Drawing.Point(0, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(984, 25);
            this.toolStrip.TabIndex = 3;
            this.toolStrip.Visible = false;
            // 
            // 新建NToolStripButton
            // 
            this.新建NToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.新建NToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("新建NToolStripButton.Image")));
            this.新建NToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.新建NToolStripButton.Name = "新建NToolStripButton";
            this.新建NToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.新建NToolStripButton.Text = "新建(&N)";
            // 
            // 打开OToolStripButton
            // 
            this.打开OToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.打开OToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("打开OToolStripButton.Image")));
            this.打开OToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.打开OToolStripButton.Name = "打开OToolStripButton";
            this.打开OToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.打开OToolStripButton.Text = "打开(&O)";
            // 
            // 保存SToolStripButton
            // 
            this.保存SToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.保存SToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("保存SToolStripButton.Image")));
            this.保存SToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.保存SToolStripButton.Name = "保存SToolStripButton";
            this.保存SToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.保存SToolStripButton.Text = "保存(&S)";
            // 
            // 打印PToolStripButton
            // 
            this.打印PToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.打印PToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("打印PToolStripButton.Image")));
            this.打印PToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.打印PToolStripButton.Name = "打印PToolStripButton";
            this.打印PToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.打印PToolStripButton.Text = "打印(&P)";
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // 剪切UToolStripButton
            // 
            this.剪切UToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.剪切UToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("剪切UToolStripButton.Image")));
            this.剪切UToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.剪切UToolStripButton.Name = "剪切UToolStripButton";
            this.剪切UToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.剪切UToolStripButton.Text = "剪切(&U)";
            // 
            // 复制CToolStripButton
            // 
            this.复制CToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.复制CToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("复制CToolStripButton.Image")));
            this.复制CToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.复制CToolStripButton.Name = "复制CToolStripButton";
            this.复制CToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.复制CToolStripButton.Text = "复制(&C)";
            // 
            // 粘贴PToolStripButton
            // 
            this.粘贴PToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.粘贴PToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("粘贴PToolStripButton.Image")));
            this.粘贴PToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.粘贴PToolStripButton.Name = "粘贴PToolStripButton";
            this.粘贴PToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.粘贴PToolStripButton.Text = "粘贴(&P)";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // 帮助LToolStripButton
            // 
            this.帮助LToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.帮助LToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("帮助LToolStripButton.Image")));
            this.帮助LToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.帮助LToolStripButton.Name = "帮助LToolStripButton";
            this.帮助LToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.帮助LToolStripButton.Text = "帮助(&L)";
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel,
            this.toolStripStatusLabelTip,
            this.toolStripStatusLabelTool,
            this.toolStripProgressBar});
            this.statusStrip.Location = new System.Drawing.Point(0, 589);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(984, 22);
            this.statusStrip.TabIndex = 4;
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStripStatusLabelTip
            // 
            this.toolStripStatusLabelTip.Name = "toolStripStatusLabelTip";
            this.toolStripStatusLabelTip.Size = new System.Drawing.Size(16, 17);
            this.toolStripStatusLabelTip.Text = "#";
            this.toolStripStatusLabelTip.DoubleClick += new System.EventHandler(this.ToolStripStatusLabelTip_DoubleClick);
            // 
            // toolStripStatusLabelTool
            // 
            this.toolStripStatusLabelTool.Enabled = false;
            this.toolStripStatusLabelTool.Name = "toolStripStatusLabelTool";
            this.toolStripStatusLabelTool.Size = new System.Drawing.Size(953, 17);
            this.toolStripStatusLabelTool.Spring = true;
            // 
            // toolStripProgressBar
            // 
            this.toolStripProgressBar.Name = "toolStripProgressBar";
            this.toolStripProgressBar.Size = new System.Drawing.Size(100, 16);
            this.toolStripProgressBar.Visible = false;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPage2);
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(984, 589);
            this.tabControl.TabIndex = 5;
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.TabControl_SelectedIndexChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.splitContainer2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(976, 563);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "答题卡";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(3, 3);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.AutoScroll = true;
            this.splitContainer2.Panel1.Controls.Add(this.groupBoxCenter);
            this.splitContainer2.Panel1.Controls.Add(this.groupBoxBlockList);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.groupBox2);
            this.splitContainer2.Size = new System.Drawing.Size(970, 557);
            this.splitContainer2.SplitterDistance = 780;
            this.splitContainer2.TabIndex = 4;
            // 
            // groupBoxCenter
            // 
            this.groupBoxCenter.Controls.Add(this.groupBoxOption);
            this.groupBoxCenter.Controls.Add(this.groupBoxExamInfo);
            this.groupBoxCenter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxCenter.Location = new System.Drawing.Point(240, 0);
            this.groupBoxCenter.Name = "groupBoxCenter";
            this.groupBoxCenter.Padding = new System.Windows.Forms.Padding(10);
            this.groupBoxCenter.Size = new System.Drawing.Size(540, 557);
            this.groupBoxCenter.TabIndex = 1;
            this.groupBoxCenter.TabStop = false;
            // 
            // groupBoxOption
            // 
            this.groupBoxOption.Controls.Add(this.panelSpan4);
            this.groupBoxOption.Controls.Add(this.panelOptionBlockObjectiveItemBold);
            this.groupBoxOption.Controls.Add(this.panelOptionBlockSubjectiveItem);
            this.groupBoxOption.Controls.Add(this.panelSpan3);
            this.groupBoxOption.Controls.Add(this.panelOptionBlockObjectiveItem);
            this.groupBoxOption.Controls.Add(this.panelSpan2);
            this.groupBoxOption.Controls.Add(this.panelOptionBlockWritting);
            this.groupBoxOption.Controls.Add(this.panelSpan1);
            this.groupBoxOption.Controls.Add(this.panel3);
            this.groupBoxOption.Controls.Add(this.panelOptionBlockName);
            this.groupBoxOption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxOption.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.groupBoxOption.Location = new System.Drawing.Point(10, 104);
            this.groupBoxOption.Name = "groupBoxOption";
            this.groupBoxOption.Padding = new System.Windows.Forms.Padding(10);
            this.groupBoxOption.Size = new System.Drawing.Size(520, 443);
            this.groupBoxOption.TabIndex = 3;
            this.groupBoxOption.TabStop = false;
            this.groupBoxOption.Text = "题块区域";
            this.groupBoxOption.Visible = false;
            // 
            // panelSpan4
            // 
            this.panelSpan4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelSpan4.Location = new System.Drawing.Point(10, 347);
            this.panelSpan4.Name = "panelSpan4";
            this.panelSpan4.Size = new System.Drawing.Size(500, 10);
            this.panelSpan4.TabIndex = 23;
            this.panelSpan4.Visible = false;
            // 
            // panelOptionBlockObjectiveItemBold
            // 
            this.panelOptionBlockObjectiveItemBold.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelOptionBlockObjectiveItemBold.Controls.Add(this.checkBoxShowBold);
            this.panelOptionBlockObjectiveItemBold.Controls.Add(this.numericUpDownBlockBoldSize);
            this.panelOptionBlockObjectiveItemBold.Controls.Add(this.label16);
            this.panelOptionBlockObjectiveItemBold.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelOptionBlockObjectiveItemBold.Location = new System.Drawing.Point(10, 357);
            this.panelOptionBlockObjectiveItemBold.Name = "panelOptionBlockObjectiveItemBold";
            this.panelOptionBlockObjectiveItemBold.Size = new System.Drawing.Size(500, 34);
            this.panelOptionBlockObjectiveItemBold.TabIndex = 22;
            this.panelOptionBlockObjectiveItemBold.Visible = false;
            this.panelOptionBlockObjectiveItemBold.VisibleChanged += new System.EventHandler(this.PanelOptionBlockObjectiveItemBold_VisibleChanged);
            // 
            // checkBoxShowBold
            // 
            this.checkBoxShowBold.AutoSize = true;
            this.checkBoxShowBold.Location = new System.Drawing.Point(16, 7);
            this.checkBoxShowBold.Name = "checkBoxShowBold";
            this.checkBoxShowBold.Size = new System.Drawing.Size(72, 16);
            this.checkBoxShowBold.TabIndex = 12;
            this.checkBoxShowBold.Text = "显示边框";
            this.checkBoxShowBold.UseVisualStyleBackColor = true;
            // 
            // numericUpDownBlockBoldSize
            // 
            this.numericUpDownBlockBoldSize.DecimalPlaces = 1;
            this.numericUpDownBlockBoldSize.Location = new System.Drawing.Point(187, 5);
            this.numericUpDownBlockBoldSize.Name = "numericUpDownBlockBoldSize";
            this.numericUpDownBlockBoldSize.Size = new System.Drawing.Size(65, 21);
            this.numericUpDownBlockBoldSize.TabIndex = 11;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(116, 9);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(65, 12);
            this.label16.TabIndex = 10;
            this.label16.Text = "边框厚度：";
            // 
            // panelOptionBlockSubjectiveItem
            // 
            this.panelOptionBlockSubjectiveItem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelOptionBlockSubjectiveItem.Controls.Add(this.richTextBoxBlockSubjectiveItem);
            this.panelOptionBlockSubjectiveItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelOptionBlockSubjectiveItem.Location = new System.Drawing.Point(10, 180);
            this.panelOptionBlockSubjectiveItem.Name = "panelOptionBlockSubjectiveItem";
            this.panelOptionBlockSubjectiveItem.Padding = new System.Windows.Forms.Padding(1);
            this.panelOptionBlockSubjectiveItem.Size = new System.Drawing.Size(500, 211);
            this.panelOptionBlockSubjectiveItem.TabIndex = 20;
            this.panelOptionBlockSubjectiveItem.Visible = false;
            this.panelOptionBlockSubjectiveItem.VisibleChanged += new System.EventHandler(this.PanelOptionBlockSubjectiveItem_VisibleChanged);
            // 
            // richTextBoxBlockSubjectiveItem
            // 
            this.richTextBoxBlockSubjectiveItem.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBoxBlockSubjectiveItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBoxBlockSubjectiveItem.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.richTextBoxBlockSubjectiveItem.Location = new System.Drawing.Point(1, 1);
            this.richTextBoxBlockSubjectiveItem.Name = "richTextBoxBlockSubjectiveItem";
            this.richTextBoxBlockSubjectiveItem.Size = new System.Drawing.Size(496, 207);
            this.richTextBoxBlockSubjectiveItem.TabIndex = 0;
            this.richTextBoxBlockSubjectiveItem.Text = "请输入题块内容";
            // 
            // panelSpan3
            // 
            this.panelSpan3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSpan3.Location = new System.Drawing.Point(10, 170);
            this.panelSpan3.Name = "panelSpan3";
            this.panelSpan3.Size = new System.Drawing.Size(500, 10);
            this.panelSpan3.TabIndex = 19;
            // 
            // panelOptionBlockObjectiveItem
            // 
            this.panelOptionBlockObjectiveItem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelOptionBlockObjectiveItem.Controls.Add(this.comboBoxObjectiveItemOrientation);
            this.panelOptionBlockObjectiveItem.Controls.Add(this.label10);
            this.panelOptionBlockObjectiveItem.Controls.Add(this.numericUpDownQuestionStart);
            this.panelOptionBlockObjectiveItem.Controls.Add(this.label9);
            this.panelOptionBlockObjectiveItem.Controls.Add(this.numericUpDownQuestionCount);
            this.panelOptionBlockObjectiveItem.Controls.Add(this.label8);
            this.panelOptionBlockObjectiveItem.Controls.Add(this.textBoxBlockOptionSequence);
            this.panelOptionBlockObjectiveItem.Controls.Add(this.label7);
            this.panelOptionBlockObjectiveItem.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelOptionBlockObjectiveItem.Location = new System.Drawing.Point(10, 112);
            this.panelOptionBlockObjectiveItem.Name = "panelOptionBlockObjectiveItem";
            this.panelOptionBlockObjectiveItem.Size = new System.Drawing.Size(500, 58);
            this.panelOptionBlockObjectiveItem.TabIndex = 18;
            this.panelOptionBlockObjectiveItem.Visible = false;
            this.panelOptionBlockObjectiveItem.VisibleChanged += new System.EventHandler(this.PanelOptionBlockObjectiveItem_VisibleChanged);
            // 
            // comboBoxObjectiveItemOrientation
            // 
            this.comboBoxObjectiveItemOrientation.FormattingEnabled = true;
            this.comboBoxObjectiveItemOrientation.Items.AddRange(new object[] {
            "横向",
            "纵向"});
            this.comboBoxObjectiveItemOrientation.Location = new System.Drawing.Point(81, 31);
            this.comboBoxObjectiveItemOrientation.Name = "comboBoxObjectiveItemOrientation";
            this.comboBoxObjectiveItemOrientation.Size = new System.Drawing.Size(66, 20);
            this.comboBoxObjectiveItemOrientation.TabIndex = 13;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(10, 35);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 12);
            this.label10.TabIndex = 12;
            this.label10.Text = "题组方向：";
            // 
            // numericUpDownQuestionStart
            // 
            this.numericUpDownQuestionStart.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.numericUpDownQuestionStart.Location = new System.Drawing.Point(431, 5);
            this.numericUpDownQuestionStart.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDownQuestionStart.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownQuestionStart.Name = "numericUpDownQuestionStart";
            this.numericUpDownQuestionStart.Size = new System.Drawing.Size(54, 21);
            this.numericUpDownQuestionStart.TabIndex = 11;
            this.numericUpDownQuestionStart.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(360, 9);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 12);
            this.label9.TabIndex = 10;
            this.label9.Text = "起始题号：";
            // 
            // numericUpDownQuestionCount
            // 
            this.numericUpDownQuestionCount.Location = new System.Drawing.Point(289, 5);
            this.numericUpDownQuestionCount.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDownQuestionCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownQuestionCount.Name = "numericUpDownQuestionCount";
            this.numericUpDownQuestionCount.Size = new System.Drawing.Size(65, 21);
            this.numericUpDownQuestionCount.TabIndex = 9;
            this.numericUpDownQuestionCount.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(218, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 12);
            this.label8.TabIndex = 8;
            this.label8.Text = "题目数量：";
            // 
            // textBoxBlockOptionSequence
            // 
            this.textBoxBlockOptionSequence.Location = new System.Drawing.Point(81, 4);
            this.textBoxBlockOptionSequence.Name = "textBoxBlockOptionSequence";
            this.textBoxBlockOptionSequence.Size = new System.Drawing.Size(120, 21);
            this.textBoxBlockOptionSequence.TabIndex = 6;
            this.textBoxBlockOptionSequence.Text = "A,B,C,D";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 8);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 12);
            this.label7.TabIndex = 5;
            this.label7.Text = "选项序列：";
            // 
            // panelSpan2
            // 
            this.panelSpan2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSpan2.Location = new System.Drawing.Point(10, 102);
            this.panelSpan2.Name = "panelSpan2";
            this.panelSpan2.Size = new System.Drawing.Size(500, 10);
            this.panelSpan2.TabIndex = 17;
            this.panelSpan2.Visible = false;
            // 
            // panelOptionBlockWritting
            // 
            this.panelOptionBlockWritting.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelOptionBlockWritting.Controls.Add(this.numericUpDownWordCount);
            this.panelOptionBlockWritting.Controls.Add(this.labelBlockOptionWordCount);
            this.panelOptionBlockWritting.Controls.Add(this.numericUpDownWordLine);
            this.panelOptionBlockWritting.Controls.Add(this.label3);
            this.panelOptionBlockWritting.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelOptionBlockWritting.Location = new System.Drawing.Point(10, 68);
            this.panelOptionBlockWritting.Name = "panelOptionBlockWritting";
            this.panelOptionBlockWritting.Size = new System.Drawing.Size(500, 34);
            this.panelOptionBlockWritting.TabIndex = 2;
            this.panelOptionBlockWritting.Visible = false;
            this.panelOptionBlockWritting.VisibleChanged += new System.EventHandler(this.PanelOptionBlockWritting_VisibleChanged);
            // 
            // numericUpDownWordCount
            // 
            this.numericUpDownWordCount.Location = new System.Drawing.Point(289, 7);
            this.numericUpDownWordCount.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.numericUpDownWordCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownWordCount.Name = "numericUpDownWordCount";
            this.numericUpDownWordCount.Size = new System.Drawing.Size(65, 21);
            this.numericUpDownWordCount.TabIndex = 11;
            this.numericUpDownWordCount.Value = new decimal(new int[] {
            600,
            0,
            0,
            0});
            this.numericUpDownWordCount.VisibleChanged += new System.EventHandler(this.NumericUpDownWordCount_VisibleChanged);
            // 
            // labelBlockOptionWordCount
            // 
            this.labelBlockOptionWordCount.AutoSize = true;
            this.labelBlockOptionWordCount.Location = new System.Drawing.Point(218, 11);
            this.labelBlockOptionWordCount.Name = "labelBlockOptionWordCount";
            this.labelBlockOptionWordCount.Size = new System.Drawing.Size(65, 12);
            this.labelBlockOptionWordCount.TabIndex = 10;
            this.labelBlockOptionWordCount.Text = "作文字数：";
            // 
            // numericUpDownWordLine
            // 
            this.numericUpDownWordLine.Location = new System.Drawing.Point(82, 7);
            this.numericUpDownWordLine.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.numericUpDownWordLine.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownWordLine.Name = "numericUpDownWordLine";
            this.numericUpDownWordLine.Size = new System.Drawing.Size(65, 21);
            this.numericUpDownWordLine.TabIndex = 9;
            this.numericUpDownWordLine.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 8;
            this.label3.Text = "作文行数：";
            // 
            // panelSpan1
            // 
            this.panelSpan1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSpan1.Location = new System.Drawing.Point(10, 58);
            this.panelSpan1.Name = "panelSpan1";
            this.panelSpan1.Size = new System.Drawing.Size(500, 10);
            this.panelSpan1.TabIndex = 3;
            this.panelSpan1.Visible = false;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.buttonBlockOptionSave);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(10, 391);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(3);
            this.panel3.Size = new System.Drawing.Size(500, 42);
            this.panel3.TabIndex = 1;
            // 
            // buttonBlockOptionSave
            // 
            this.buttonBlockOptionSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonBlockOptionSave.Location = new System.Drawing.Point(331, 3);
            this.buttonBlockOptionSave.Name = "buttonBlockOptionSave";
            this.buttonBlockOptionSave.Size = new System.Drawing.Size(166, 36);
            this.buttonBlockOptionSave.TabIndex = 3;
            this.buttonBlockOptionSave.Text = "保存题块";
            this.buttonBlockOptionSave.UseVisualStyleBackColor = true;
            this.buttonBlockOptionSave.Click += new System.EventHandler(this.ButtonBlockOptionSave_Click);
            // 
            // panelOptionBlockName
            // 
            this.panelOptionBlockName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelOptionBlockName.Controls.Add(this.checkBoxBlockNameTitleCenter);
            this.panelOptionBlockName.Controls.Add(this.label2);
            this.panelOptionBlockName.Controls.Add(this.textBoxBlockNameTitle);
            this.panelOptionBlockName.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelOptionBlockName.Location = new System.Drawing.Point(10, 24);
            this.panelOptionBlockName.Name = "panelOptionBlockName";
            this.panelOptionBlockName.Size = new System.Drawing.Size(500, 34);
            this.panelOptionBlockName.TabIndex = 0;
            // 
            // checkBoxBlockNameTitleCenter
            // 
            this.checkBoxBlockNameTitleCenter.AutoSize = true;
            this.checkBoxBlockNameTitleCenter.Location = new System.Drawing.Point(413, 7);
            this.checkBoxBlockNameTitleCenter.Name = "checkBoxBlockNameTitleCenter";
            this.checkBoxBlockNameTitleCenter.Size = new System.Drawing.Size(72, 16);
            this.checkBoxBlockNameTitleCenter.TabIndex = 4;
            this.checkBoxBlockNameTitleCenter.Text = "标题居中";
            this.checkBoxBlockNameTitleCenter.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "题目标题：";
            // 
            // textBoxBlockNameTitle
            // 
            this.textBoxBlockNameTitle.Location = new System.Drawing.Point(81, 6);
            this.textBoxBlockNameTitle.Name = "textBoxBlockNameTitle";
            this.textBoxBlockNameTitle.Size = new System.Drawing.Size(317, 21);
            this.textBoxBlockNameTitle.TabIndex = 2;
            // 
            // groupBoxExamInfo
            // 
            this.groupBoxExamInfo.Controls.Add(this.label1);
            this.groupBoxExamInfo.Controls.Add(this.textBoxExamName);
            this.groupBoxExamInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxExamInfo.Location = new System.Drawing.Point(10, 24);
            this.groupBoxExamInfo.Name = "groupBoxExamInfo";
            this.groupBoxExamInfo.Size = new System.Drawing.Size(520, 80);
            this.groupBoxExamInfo.TabIndex = 2;
            this.groupBoxExamInfo.TabStop = false;
            this.groupBoxExamInfo.Text = "考试信息";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "考试名称：";
            // 
            // textBoxExamName
            // 
            this.textBoxExamName.Location = new System.Drawing.Point(92, 31);
            this.textBoxExamName.Name = "textBoxExamName";
            this.textBoxExamName.Size = new System.Drawing.Size(418, 21);
            this.textBoxExamName.TabIndex = 0;
            this.textBoxExamName.Text = "考试名称";
            // 
            // groupBoxBlockList
            // 
            this.groupBoxBlockList.Controls.Add(this.buttonAddNodeBlock);
            this.groupBoxBlockList.Controls.Add(this.panelTreeViewOption);
            this.groupBoxBlockList.Controls.Add(this.treeViewBlock);
            this.groupBoxBlockList.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBoxBlockList.Location = new System.Drawing.Point(0, 0);
            this.groupBoxBlockList.Name = "groupBoxBlockList";
            this.groupBoxBlockList.Padding = new System.Windows.Forms.Padding(10);
            this.groupBoxBlockList.Size = new System.Drawing.Size(240, 557);
            this.groupBoxBlockList.TabIndex = 0;
            this.groupBoxBlockList.TabStop = false;
            this.groupBoxBlockList.Text = "题块目录";
            // 
            // buttonAddNodeBlock
            // 
            this.buttonAddNodeBlock.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonAddNodeBlock.Location = new System.Drawing.Point(10, 511);
            this.buttonAddNodeBlock.Name = "buttonAddNodeBlock";
            this.buttonAddNodeBlock.Size = new System.Drawing.Size(192, 36);
            this.buttonAddNodeBlock.TabIndex = 0;
            this.buttonAddNodeBlock.Text = "新建题块";
            this.buttonAddNodeBlock.UseVisualStyleBackColor = true;
            this.buttonAddNodeBlock.Click += new System.EventHandler(this.ButtonAddNodeBlock_Click);
            // 
            // panelTreeViewOption
            // 
            this.panelTreeViewOption.Controls.Add(this.buttonNodeDelete);
            this.panelTreeViewOption.Controls.Add(this.buttonNodeDown);
            this.panelTreeViewOption.Controls.Add(this.buttonNodeUp);
            this.panelTreeViewOption.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelTreeViewOption.Location = new System.Drawing.Point(202, 24);
            this.panelTreeViewOption.Name = "panelTreeViewOption";
            this.panelTreeViewOption.Size = new System.Drawing.Size(28, 523);
            this.panelTreeViewOption.TabIndex = 2;
            // 
            // buttonNodeDelete
            // 
            this.buttonNodeDelete.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonNodeDelete.Location = new System.Drawing.Point(0, 46);
            this.buttonNodeDelete.Name = "buttonNodeDelete";
            this.buttonNodeDelete.Size = new System.Drawing.Size(28, 23);
            this.buttonNodeDelete.TabIndex = 3;
            this.buttonNodeDelete.Text = "X";
            this.buttonNodeDelete.UseVisualStyleBackColor = true;
            this.buttonNodeDelete.Click += new System.EventHandler(this.ButtonNodeDelete_Click);
            // 
            // buttonNodeDown
            // 
            this.buttonNodeDown.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonNodeDown.Location = new System.Drawing.Point(0, 23);
            this.buttonNodeDown.Name = "buttonNodeDown";
            this.buttonNodeDown.Size = new System.Drawing.Size(28, 23);
            this.buttonNodeDown.TabIndex = 2;
            this.buttonNodeDown.Text = "↓";
            this.buttonNodeDown.UseVisualStyleBackColor = true;
            this.buttonNodeDown.Click += new System.EventHandler(this.ButtonNodeDown_Click);
            // 
            // buttonNodeUp
            // 
            this.buttonNodeUp.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonNodeUp.Location = new System.Drawing.Point(0, 0);
            this.buttonNodeUp.Name = "buttonNodeUp";
            this.buttonNodeUp.Size = new System.Drawing.Size(28, 23);
            this.buttonNodeUp.TabIndex = 1;
            this.buttonNodeUp.Text = "↑";
            this.buttonNodeUp.UseVisualStyleBackColor = true;
            this.buttonNodeUp.Click += new System.EventHandler(this.ButtonNodeUp_Click);
            // 
            // treeViewBlock
            // 
            this.treeViewBlock.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.treeViewBlock.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeViewBlock.Location = new System.Drawing.Point(10, 24);
            this.treeViewBlock.Name = "treeViewBlock";
            this.treeViewBlock.Size = new System.Drawing.Size(220, 523);
            this.treeViewBlock.TabIndex = 0;
            this.treeViewBlock.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.TreeViewBlock_AfterSelect);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox8);
            this.groupBox2.Controls.Add(this.groupBoxBlockType);
            this.groupBox2.Controls.Add(this.buttonPreviewFile);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(10);
            this.groupBox2.Size = new System.Drawing.Size(186, 557);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.comboBoxPageSize);
            this.groupBox8.Controls.Add(this.comboBoxPageOrientation);
            this.groupBox8.Controls.Add(this.label5);
            this.groupBox8.Controls.Add(this.label4);
            this.groupBox8.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox8.Location = new System.Drawing.Point(10, 363);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(166, 98);
            this.groupBox8.TabIndex = 4;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "页面布局：";
            // 
            // comboBoxPageSize
            // 
            this.comboBoxPageSize.FormattingEnabled = true;
            this.comboBoxPageSize.Items.AddRange(new object[] {
            "A4",
            "A3"});
            this.comboBoxPageSize.Location = new System.Drawing.Point(85, 20);
            this.comboBoxPageSize.Name = "comboBoxPageSize";
            this.comboBoxPageSize.Size = new System.Drawing.Size(76, 20);
            this.comboBoxPageSize.TabIndex = 6;
            this.comboBoxPageSize.SelectedIndexChanged += new System.EventHandler(this.ComboBoxPageSize_SelectedIndexChanged);
            // 
            // comboBoxPageOrientation
            // 
            this.comboBoxPageOrientation.FormattingEnabled = true;
            this.comboBoxPageOrientation.Items.AddRange(new object[] {
            "横向",
            "纵向"});
            this.comboBoxPageOrientation.Location = new System.Drawing.Point(85, 55);
            this.comboBoxPageOrientation.Name = "comboBoxPageOrientation";
            this.comboBoxPageOrientation.Size = new System.Drawing.Size(76, 20);
            this.comboBoxPageOrientation.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 12);
            this.label5.TabIndex = 5;
            this.label5.Text = "纸张大小：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 4;
            this.label4.Text = "纸张方向：";
            // 
            // groupBoxBlockType
            // 
            this.groupBoxBlockType.Controls.Add(this.buttonChineseWriting);
            this.groupBoxBlockType.Controls.Add(this.panel9);
            this.groupBoxBlockType.Controls.Add(this.buttonEnglishWriting);
            this.groupBoxBlockType.Controls.Add(this.panel8);
            this.groupBoxBlockType.Controls.Add(this.buttonSubjectiveItem);
            this.groupBoxBlockType.Controls.Add(this.panel2);
            this.groupBoxBlockType.Controls.Add(this.buttonObjectiveItem);
            this.groupBoxBlockType.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxBlockType.Enabled = false;
            this.groupBoxBlockType.Location = new System.Drawing.Point(10, 24);
            this.groupBoxBlockType.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.groupBoxBlockType.Name = "groupBoxBlockType";
            this.groupBoxBlockType.Padding = new System.Windows.Forms.Padding(10);
            this.groupBoxBlockType.Size = new System.Drawing.Size(166, 339);
            this.groupBoxBlockType.TabIndex = 3;
            this.groupBoxBlockType.TabStop = false;
            this.groupBoxBlockType.Text = "题型：";
            // 
            // buttonChineseWriting
            // 
            this.buttonChineseWriting.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonChineseWriting.Location = new System.Drawing.Point(10, 204);
            this.buttonChineseWriting.Name = "buttonChineseWriting";
            this.buttonChineseWriting.Size = new System.Drawing.Size(146, 42);
            this.buttonChineseWriting.TabIndex = 17;
            this.buttonChineseWriting.Text = "语文作文";
            this.buttonChineseWriting.UseVisualStyleBackColor = true;
            this.buttonChineseWriting.Click += new System.EventHandler(this.ButtonChineseWriting_Click);
            // 
            // panel9
            // 
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(10, 186);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(146, 18);
            this.panel9.TabIndex = 16;
            // 
            // buttonEnglishWriting
            // 
            this.buttonEnglishWriting.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonEnglishWriting.Location = new System.Drawing.Point(10, 144);
            this.buttonEnglishWriting.Name = "buttonEnglishWriting";
            this.buttonEnglishWriting.Size = new System.Drawing.Size(146, 42);
            this.buttonEnglishWriting.TabIndex = 9;
            this.buttonEnglishWriting.Text = "英语作文";
            this.buttonEnglishWriting.UseVisualStyleBackColor = true;
            this.buttonEnglishWriting.Click += new System.EventHandler(this.ButtonEnglishWriting_Click);
            // 
            // panel8
            // 
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(10, 126);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(146, 18);
            this.panel8.TabIndex = 15;
            // 
            // buttonSubjectiveItem
            // 
            this.buttonSubjectiveItem.BackColor = System.Drawing.Color.Transparent;
            this.buttonSubjectiveItem.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonSubjectiveItem.Font = new System.Drawing.Font("宋体", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.buttonSubjectiveItem.ForeColor = System.Drawing.Color.Black;
            this.buttonSubjectiveItem.Location = new System.Drawing.Point(10, 84);
            this.buttonSubjectiveItem.Name = "buttonSubjectiveItem";
            this.buttonSubjectiveItem.Size = new System.Drawing.Size(146, 42);
            this.buttonSubjectiveItem.TabIndex = 12;
            this.buttonSubjectiveItem.Text = "主观题";
            this.buttonSubjectiveItem.UseVisualStyleBackColor = false;
            this.buttonSubjectiveItem.Click += new System.EventHandler(this.ButtonSubjectiveItem_Click);
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(10, 66);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(146, 18);
            this.panel2.TabIndex = 11;
            // 
            // buttonObjectiveItem
            // 
            this.buttonObjectiveItem.BackColor = System.Drawing.Color.Transparent;
            this.buttonObjectiveItem.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonObjectiveItem.Font = new System.Drawing.Font("宋体", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.buttonObjectiveItem.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonObjectiveItem.Location = new System.Drawing.Point(10, 24);
            this.buttonObjectiveItem.Name = "buttonObjectiveItem";
            this.buttonObjectiveItem.Size = new System.Drawing.Size(146, 42);
            this.buttonObjectiveItem.TabIndex = 10;
            this.buttonObjectiveItem.Text = "客观题";
            this.buttonObjectiveItem.UseVisualStyleBackColor = false;
            this.buttonObjectiveItem.Click += new System.EventHandler(this.ButtonObjectiveItem_Click);
            // 
            // buttonPreviewFile
            // 
            this.buttonPreviewFile.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonPreviewFile.Location = new System.Drawing.Point(10, 511);
            this.buttonPreviewFile.Name = "buttonPreviewFile";
            this.buttonPreviewFile.Size = new System.Drawing.Size(166, 36);
            this.buttonPreviewFile.TabIndex = 2;
            this.buttonPreviewFile.Text = "预览文件";
            this.buttonPreviewFile.UseVisualStyleBackColor = true;
            this.buttonPreviewFile.Click += new System.EventHandler(this.ButtonPreviewFile_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.splitContainer1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(976, 563);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "预览";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 3);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.docDocumentViewer);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBox1);
            this.splitContainer1.Size = new System.Drawing.Size(970, 557);
            this.splitContainer1.SplitterDistance = 780;
            this.splitContainer1.TabIndex = 3;
            // 
            // docDocumentViewer
            // 
            this.docDocumentViewer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.docDocumentViewer.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.docDocumentViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.docDocumentViewer.EnableHandTools = false;
            this.docDocumentViewer.Location = new System.Drawing.Point(0, 0);
            this.docDocumentViewer.Name = "docDocumentViewer";
            this.docDocumentViewer.Size = new System.Drawing.Size(780, 557);
            this.docDocumentViewer.TabIndex = 0;
            this.docDocumentViewer.Text = "docDocumentViewer1";
            this.docDocumentViewer.ToPdfParameterList = null;
            this.docDocumentViewer.ZoomMode = Spire.DocViewer.Forms.ZoomMode.Default;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.buttonZoomToDown);
            this.groupBox1.Controls.Add(this.buttonZoomToUp);
            this.groupBox1.Controls.Add(this.buttonExportFile);
            this.groupBox1.Controls.Add(this.buttonExit);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(20);
            this.groupBox1.Size = new System.Drawing.Size(186, 557);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "操作...";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.Dock = System.Windows.Forms.DockStyle.Top;
            this.button1.Font = new System.Drawing.Font("宋体", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Location = new System.Drawing.Point(20, 70);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(146, 36);
            this.button1.TabIndex = 6;
            this.button1.Text = "打开文件目录";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // buttonZoomToDown
            // 
            this.buttonZoomToDown.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.buttonZoomToDown.Location = new System.Drawing.Point(62, 112);
            this.buttonZoomToDown.Name = "buttonZoomToDown";
            this.buttonZoomToDown.Size = new System.Drawing.Size(36, 39);
            this.buttonZoomToDown.TabIndex = 5;
            this.buttonZoomToDown.Text = "-";
            this.buttonZoomToDown.UseVisualStyleBackColor = true;
            this.buttonZoomToDown.Click += new System.EventHandler(this.ButtonZoomToDown_Click);
            // 
            // buttonZoomToUp
            // 
            this.buttonZoomToUp.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.buttonZoomToUp.Location = new System.Drawing.Point(20, 112);
            this.buttonZoomToUp.Name = "buttonZoomToUp";
            this.buttonZoomToUp.Size = new System.Drawing.Size(36, 39);
            this.buttonZoomToUp.TabIndex = 4;
            this.buttonZoomToUp.Text = "+";
            this.buttonZoomToUp.UseVisualStyleBackColor = true;
            this.buttonZoomToUp.Click += new System.EventHandler(this.ButtonZoomToUp_Click);
            // 
            // buttonExportFile
            // 
            this.buttonExportFile.BackColor = System.Drawing.Color.Transparent;
            this.buttonExportFile.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonExportFile.Font = new System.Drawing.Font("宋体", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.buttonExportFile.ForeColor = System.Drawing.Color.Black;
            this.buttonExportFile.Location = new System.Drawing.Point(20, 34);
            this.buttonExportFile.Name = "buttonExportFile";
            this.buttonExportFile.Size = new System.Drawing.Size(146, 36);
            this.buttonExportFile.TabIndex = 3;
            this.buttonExportFile.Text = "导出文件";
            this.buttonExportFile.UseVisualStyleBackColor = false;
            this.buttonExportFile.Click += new System.EventHandler(this.ButtonExportFile_Click);
            // 
            // buttonExit
            // 
            this.buttonExit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonExit.Location = new System.Drawing.Point(20, 498);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(146, 39);
            this.buttonExit.TabIndex = 2;
            this.buttonExit.Text = "退出";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.ButtonExit_Click);
            // 
            // ApplicationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 611);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.toolStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1000, 650);
            this.Name = "ApplicationForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "答题卡工具";
            this.Load += new System.EventHandler(this.ApplicationForm_Load);
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.groupBoxCenter.ResumeLayout(false);
            this.groupBoxOption.ResumeLayout(false);
            this.panelOptionBlockObjectiveItemBold.ResumeLayout(false);
            this.panelOptionBlockObjectiveItemBold.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBlockBoldSize)).EndInit();
            this.panelOptionBlockSubjectiveItem.ResumeLayout(false);
            this.panelOptionBlockObjectiveItem.ResumeLayout(false);
            this.panelOptionBlockObjectiveItem.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownQuestionStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownQuestionCount)).EndInit();
            this.panelOptionBlockWritting.ResumeLayout(false);
            this.panelOptionBlockWritting.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownWordCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownWordLine)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panelOptionBlockName.ResumeLayout(false);
            this.panelOptionBlockName.PerformLayout();
            this.groupBoxExamInfo.ResumeLayout(false);
            this.groupBoxExamInfo.PerformLayout();
            this.groupBoxBlockList.ResumeLayout(false);
            this.panelTreeViewOption.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBoxBlockType.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripButton 新建NToolStripButton;
        private System.Windows.Forms.ToolStripButton 打开OToolStripButton;
        private System.Windows.Forms.ToolStripButton 保存SToolStripButton;
        private System.Windows.Forms.ToolStripButton 打印PToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripButton 剪切UToolStripButton;
        private System.Windows.Forms.ToolStripButton 复制CToolStripButton;
        private System.Windows.Forms.ToolStripButton 粘贴PToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton 帮助LToolStripButton;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelTip;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.GroupBox groupBoxCenter;
        private System.Windows.Forms.GroupBox groupBoxBlockList;
        private System.Windows.Forms.Button buttonAddNodeBlock;
        private System.Windows.Forms.Panel panelTreeViewOption;
        private System.Windows.Forms.Button buttonNodeDelete;
        private System.Windows.Forms.Button buttonNodeDown;
        private System.Windows.Forms.Button buttonNodeUp;
        private System.Windows.Forms.TreeView treeViewBlock;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.ComboBox comboBoxPageSize;
        private System.Windows.Forms.ComboBox comboBoxPageOrientation;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBoxBlockType;
        private System.Windows.Forms.Button buttonChineseWriting;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Button buttonEnglishWriting;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Button buttonSubjectiveItem;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button buttonObjectiveItem;
        private System.Windows.Forms.Button buttonPreviewFile;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonExportFile;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.Button buttonZoomToUp;
        private System.Windows.Forms.Button buttonZoomToDown;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelTool;
        private Spire.DocViewer.Forms.DocDocumentViewer docDocumentViewer;
        private System.Windows.Forms.GroupBox groupBoxOption;
        private System.Windows.Forms.GroupBox groupBoxExamInfo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxExamName;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button buttonBlockOptionSave;
        private System.Windows.Forms.Panel panelOptionBlockName;
        private System.Windows.Forms.CheckBox checkBoxBlockNameTitleCenter;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxBlockNameTitle;
        private System.Windows.Forms.Panel panelSpan1;
        private System.Windows.Forms.Panel panelOptionBlockSubjectiveItem;
        private System.Windows.Forms.Panel panelSpan3;
        private System.Windows.Forms.Panel panelOptionBlockObjectiveItem;
        private System.Windows.Forms.ComboBox comboBoxObjectiveItemOrientation;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown numericUpDownQuestionStart;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown numericUpDownQuestionCount;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxBlockOptionSequence;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panelSpan2;
        private System.Windows.Forms.Panel panelOptionBlockWritting;
        private System.Windows.Forms.NumericUpDown numericUpDownWordCount;
        private System.Windows.Forms.Label labelBlockOptionWordCount;
        private System.Windows.Forms.NumericUpDown numericUpDownWordLine;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RichTextBox richTextBoxBlockSubjectiveItem;
        private System.Windows.Forms.Panel panelSpan4;
        private System.Windows.Forms.Panel panelOptionBlockObjectiveItemBold;
        private System.Windows.Forms.NumericUpDown numericUpDownBlockBoldSize;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.CheckBox checkBoxShowBold;
        private System.Windows.Forms.Button button1;
    }
}

